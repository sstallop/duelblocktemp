﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{
    public string color;
    private bool chained;
    private bool dieNormal;
    private ParticleSystem pSystem;
    private Animation anim;

    public void Start()
    {
        pSystem = GetComponent<ParticleSystem>();
        chained = false;
        dieNormal = true;
        anim = GetComponent<Animation>();
    }

    public void death()
    {
        anim.Play("death");
        pSystem.Play();

        if (!pSystem.loop)
        {
            Destroy(gameObject, pSystem.duration);
        }
        
    }

    public void explode()
    {
        anim.Play("explode");
        Destroy(gameObject, 0.5f);
    }

    public void setChained(bool chained)
    {
        this.chained = chained;
    }

    public bool getChained()
    {
        return chained;
    }

    public void setDieNormal(bool dieNormal)
    {
        this.dieNormal = dieNormal;
    }

    public bool getDieNormal()
    {
        return dieNormal;
    }


}


