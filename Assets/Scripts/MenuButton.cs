﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButton : MonoBehaviour {

    private Animation anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animation>();
	}

    public void onHover()
    {
        Debug.Log(anim.GetClipCount());
        anim.Play("onMouseYes");
    }

    public void offHover()
    {
        anim.Play("offMouseYes");
    }


    void Update ()
    {
		
	}
}
