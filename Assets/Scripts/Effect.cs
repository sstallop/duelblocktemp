﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {

    public string effectName;
    public AudioClip sound;
    private AudioSource source;

    Vector3 gridOverlayPositionP1 = new Vector3(4, 7, -4);
    Vector3 gridOverlayPositionP2 = new Vector3(22, 7, -4);

    public string getName()
    {
        source = GetComponent<AudioSource>();
        return name;
    }

    public void activateBomb(Transform[,] gridArray, int x, int y, int gridWidth, int gridHeight)
    {
        Vector3 effPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
        Instantiate(Resources.Load("ParticleClusters/ExplosionParticles", typeof(GameObject)), effPosition, Quaternion.identity);
        source.PlayOneShot(sound);

            for (int a = -2; a <= 2; a++)
            {
                for (int b = -2; b <= 2; b++)
                {
                    if (x + a > 0 && x + a < gridWidth && y + b > 0 && y + b < gridHeight)
                    {
                        if (gridArray[x + a, y + b] != null)
                        {
                            if (gridArray[x + a, y + b].GetComponent<Block>() != null)
                            {
                                gridArray[x + a, y + b].GetComponent<Block>().setDieNormal(false);
                                gridArray[x + a, y + b].GetComponent<Block>().explode();
                            }
                            else if (gridArray[x + a, y + b].GetComponent<GarbageFinal>() != null)
                            {
                                gridArray[x + a, y + b].GetComponent<GarbageFinal>().setDieNormal(false);
                                gridArray[x + a, y + b].GetComponent<GarbageFinal>().explode();
                            }

                        }
                    }
                }
            }
        
    }

    public void activateShield(int offset, int gridWidth, int gridHeight)
    {
        Vector3 effPosition = new Vector3(4 + offset, gridHeight, transform.position.z - 1);
        Instantiate(Resources.Load("ParticleClusters/ShieldParticles", typeof(GameObject)), effPosition, Quaternion.identity);
    }

    public void activateSpark(Transform[,] gridArray, int x, int y, int offset, int gridWidth, int gridHeight)
    {
        Vector3 effPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
        GameObject spark = (GameObject)Instantiate(Resources.Load("ParticleClusters/SparkParticles", typeof(GameObject)), effPosition, Quaternion.identity);
        spark.GetComponent<SparkScript>().setUp(x, y, offset, gridWidth, gridHeight);
        source.PlayOneShot(sound);

        for (int a = 0; a < gridWidth; a++)
        {
            if (gridArray[a, y] != null)
            {
                if (gridArray[a, y].GetComponent<Block>() != null)
                {
                    gridArray[a, y].GetComponent<Block>().setDieNormal(false);
                    gridArray[a, y].GetComponent<Block>().explode();
                }
            }
        }

        for (int b = 0; b < gridHeight; b++)
        {
            if (gridArray[x, b] != null)
            {
                if (gridArray[x, b].GetComponent<Block>() != null)
                {
                    gridArray[x, b].GetComponent<Block>().setDieNormal(false);
                    gridArray[x, b].GetComponent<Block>().explode();
                }
            }
        }

    }

    public void activateAttack(int player)
    {
        Vector3 effPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
        Instantiate(Resources.Load("ParticleClusters/AttackParticles", typeof(GameObject)), effPosition, Quaternion.identity);
        source.PlayOneShot(sound);

        

        if (player == 1)
        {
            GameObject.Find("GridP2").GetComponent<PlayingGrid>().addGarbage(3);
        }
        else if(player == 2)
        {
            GameObject.Find("GridP1").GetComponent<PlayingGrid>().addGarbage(3);

        }
    }

    public void activateKamikaze(int player)
    {

        source.PlayOneShot(sound);

        if(player == 1)
        {
            GameObject.Find("GridP1").GetComponent<PlayingGrid>().addGarbage(7);
            GameObject.Find("GridP2").GetComponent<PlayingGrid>().addGarbage(7);
        }
        else if(player == 2)
        {
            GameObject.Find("GridP1").GetComponent<PlayingGrid>().addGarbage(7);
            GameObject.Find("GridP2").GetComponent<PlayingGrid>().addGarbage(7);
        }

    }

    public void activateHeal(int player)
    {
        source.PlayOneShot(sound);

        if (player == 1)
        {

            if (GameObject.Find("GridP1").GetComponent<PlayingGrid>().getGarbage() >= 10)
                GameObject.Find("GridP1").GetComponent<PlayingGrid>().removeGarbage(10);
            else
                GameObject.Find("GridP1").GetComponent<PlayingGrid>().removeGarbage(GameObject.Find("GridP1").GetComponent<PlayingGrid>().getGarbage());
        }
        else if (player == 2)
        {

            if (GameObject.Find("GridP2").GetComponent<PlayingGrid>().getGarbage() >= 10)
                GameObject.Find("GridP2").GetComponent<PlayingGrid>().removeGarbage(10);
            else
                GameObject.Find("GridP2").GetComponent<PlayingGrid>().removeGarbage(GameObject.Find("GridP2").GetComponent<PlayingGrid>().getGarbage());
        }
    }

    public void activateBurn(int player)
    {
        source.PlayOneShot(sound);

        if (player == 1)
        {
            Vector3 effPosition = new Vector3(4 + 18, 0, transform.position.z + 4);
            Instantiate(Resources.Load("ParticleClusters/BurnParticles", typeof(GameObject)), effPosition, Quaternion.identity);
            GameObject.Find("GridP2").GetComponent<PlayingGrid>().setBurnTime(Time.time);
        }
        else if(player == 2)
        {
            Vector3 effPosition = new Vector3(4, 0, transform.position.z + 4);
            Instantiate(Resources.Load("ParticleClusters/BurnParticles", typeof(GameObject)), effPosition, Quaternion.identity);
            GameObject.Find("GridP1").GetComponent<PlayingGrid>().setBurnTime(Time.time);
        }
    }
}
