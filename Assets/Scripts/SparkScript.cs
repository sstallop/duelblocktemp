﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SparkScript : MonoBehaviour {

	void Start ()
    {
        Destroy(gameObject, 0.75f);
    }
	
	public void setUp(int x, int y, int offset, int gridWidth, int gridHeight)
    {
        
        ParticleSystem.VelocityOverLifetimeModule leftVel = transform.Find("LineLeft").GetComponent<ParticleSystem>().velocityOverLifetime;
        ParticleSystem.MinMaxCurve rateLeft = new ParticleSystem.MinMaxCurve();
        rateLeft.constantMax = -3*(x-1);
        leftVel.x = rateLeft;

        ParticleSystem.VelocityOverLifetimeModule rightVel = transform.Find("LineRight").GetComponent<ParticleSystem>().velocityOverLifetime;
        ParticleSystem.MinMaxCurve rateRight = new ParticleSystem.MinMaxCurve();
        rateRight.constantMax = 3 * (gridWidth - x);
        rightVel.x = rateRight;

        ParticleSystem.VelocityOverLifetimeModule downVel = transform.Find("LineDown").GetComponent<ParticleSystem>().velocityOverLifetime;
        ParticleSystem.MinMaxCurve rateDown = new ParticleSystem.MinMaxCurve();
        rateDown.constantMax = -3 * (y-1);
        downVel.y = rateDown;

        ParticleSystem.VelocityOverLifetimeModule upVel = transform.Find("LineUp").GetComponent<ParticleSystem>().velocityOverLifetime;
        ParticleSystem.MinMaxCurve rateUp = new ParticleSystem.MinMaxCurve();
        rateUp.constantMax = 3 * (gridHeight - y);
        upVel.y = rateUp;
        


    }
}
