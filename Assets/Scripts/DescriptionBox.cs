﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DescriptionBox : MonoBehaviour
{
    private Text title;
    private Text description;
    private Text cost;

    private GameObject nameObject;
    private GameObject descriptionObject;
    private GameObject creditCost;

    void Start()
    {
        nameObject = transform.Find("effectname").gameObject;
        descriptionObject = transform.Find("effectdescription").gameObject;
        creditCost = transform.Find("creditCost").gameObject;

        title = nameObject.GetComponent<Text>();
        description = descriptionObject.GetComponent<Text>();
        cost = creditCost.GetComponent<Text>();

    }

    void Update()
    {

    }

    public void setName(string title)
    {
        
        nameObject = transform.Find("effectname").gameObject;
        this.title = nameObject.GetComponent<Text>();
        this.title.text = title;
    }

    public void setDescription(string description)
    {
        descriptionObject = transform.Find("effectdescription").gameObject;
        this.description = descriptionObject.GetComponent<Text>();
        this.description.text = description;
    }

    public void setCost(string cost)
    {
        creditCost = transform.Find("creditCost").gameObject;
        this.cost = creditCost.GetComponent<Text>();
        this.cost.text = cost;
    }

}
