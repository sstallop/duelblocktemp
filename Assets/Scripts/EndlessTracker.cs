﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndlessTracker : MonoBehaviour {

    private int score;
    private int level;
    private string mode;

    private int left;

    private bool timerOn;
    private float timer;
    private float timerGap;

    private TextMeshPro meshScore;
    private TextMeshPro meshLevel;
    private TextMeshPro meshRemaining;
    private TextMeshPro timeLeft;

    private EndlessPlayer p;

    void Start()
    {
        mode = "";
        score = 0;
        level = 0;
        left = 0;

        timer = 0;
        timerGap = 0;

        meshScore = transform.Find("ScoreText").gameObject.GetComponent<TextMeshPro>();
        meshLevel = transform.Find("LevelText").gameObject.GetComponent<TextMeshPro>();
        meshRemaining = transform.Find("RemainingText").gameObject.GetComponent<TextMeshPro>();
        timeLeft = transform.Find("TimeComponent").gameObject.GetComponent<TextMeshPro>();

        p = FindObjectOfType<EndlessPlayer>();

        timerOn = false;
    }

    void Update()
    {
        meshScore.text = "Score: " + score;



        if (mode.Equals("Rush"))
        {
            rushProtocol();
        }
        else if (mode.Equals("Endless"))
        {
            endlessProtocol();
        }

        if(p.getGameOver() == true)
        {
            timerOn = false;
        }
    }

    private void rushProtocol()
    {
        meshLevel.text = "Level: " + level;
        meshRemaining.text = "Remaining: " + (level * 5000 - score);
        timeLeft.text = "Time: " + timer;

        if (score >= level * 5000)
        {
            level++;
        }

        if (timerOn == true)
            if (Time.time - timerGap > 5f / (level+5))
            {
                timerGap = Time.time;
                timer--;
            }

 

        if (timer <= 0)
        {
            p.setGameOver(true);
        }
    }

    private void endlessProtocol()
    {
        meshLevel.text = "Level: NA";
        meshRemaining.text = "Remaining: NA";
        timeLeft.text = "Time: NA";
    }

    public void setUp(string mode)
    {
        setScore(0);
        setLevel(1);

        this.mode = mode;

        if (mode.Equals("Rush"))
        {
            timerOn = true;
            timer = 99;
            timerGap = Time.time;
        }
        else if(mode.Equals("Endless"))
        {
            timerOn = false;
            timer = -1;
            timerGap = Time.time;
        }


    }

    public void addTime(float time)
    {
        timer += time;
    }

    public float getTimeLeft()
    {
        return timer;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    public int getScore()
    {
        return score;
    }

    public void setLevel(int level)
    {
        this.level = level;
    }

    public int getLevel()
    {
        return level;
    }


    public float getFastFallSpeed()
    {
        return 0.025f;
    }

    public float getSlowFallSpeed()
    {
        return 2.5f/(level + 5);
    }

}
