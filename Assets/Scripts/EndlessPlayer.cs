﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndlessPlayer : PlayingGrid {

    private GridCover cover;
    private float originalComboPitch;
	// Use this for initialization
	void Start ()
    {
        cover = FindObjectOfType<GridCover>();
        source = GetComponent<AudioSource>();

        comboKing = GetComponents<AudioSource>()[1];
        originalComboPitch = comboKing.pitch;

        gameOver = false;
    }

    public void updateGrid(EndlessBlockGroup blockGroup)
    {
        for (int y = 1; y < gridHeight; y++)
        {
            for (int x = 1; x < gridWidth; x++)
            {
                if (gridArray[x, y] != null)
                {
                    if (gridArray[x, y].parent == blockGroup.transform)
                    {
                        gridArray[x, y] = null;
                    }
                }

            }
        }

        foreach (Transform t in blockGroup.transform)
        {
            Vector2 pos = round(t.position);

            if (pos.y < gridHeight)
            {
                gridArray[(int)pos.x, (int)pos.y] = t;
            }
        }
    }

    public void resolveCombos()
    {
        comboRoutine = comboFix();
        StartCoroutine(comboRoutine);
    }



    private IEnumerator comboFix()
    {
        List<BlockChain> unresolved = findAllChains();

        int combo = 0;

        while (unresolved.Count > 0)
        {
            combo++;

            Vector3 effPosition = new Vector3(4 + offset, 12, transform.position.z - 5);
            GameObject comboObject = (GameObject)Instantiate(Resources.Load("ParticleClusters/ComboEffect", typeof(GameObject)), effPosition, Quaternion.identity);
            comboObject.GetComponentInChildren<TextMeshPro>().text = combo + " combo!";

            comboKing.pitch = originalComboPitch + (0.1f * (combo - 1));
            comboKing.PlayOneShot(chainBreak);

            int totalBreak = 0;

            setChains(unresolved);
            totalBreak += destroyChains();

            int score = (int)(totalBreak * 25 * Mathf.Pow(3, combo - 1) + 200 * (combo - 1));

            FindObjectOfType<EndlessTracker>().setScore(FindObjectOfType<EndlessTracker>().getScore() + score);
            FindObjectOfType<EndlessTracker>().addTime(totalBreak * combo);

            yield return new WaitForSeconds(1.25f);
            unresolved = findAllChains();


        }

        if (gridArray[4, 14] != null)
        {
            gameOver = true;
        }
        if (!gameOver)
        {            
            spawnNextBlockGroup();

            int totalOccupied = 0;

            for(int i = 0; i < gridWidth; i++)
            {
                for(int j = 0; j < gridHeight; j++)
                {
                    if (gridArray[i, j] != null)
                    {
                        totalOccupied++;
                    }
                }
            }

            if(totalOccupied >= 50 && cover.getPeril() == false)
            {
                cover.setPeril(true);
            }
            else if(totalOccupied < 50 && cover.getPeril() == true)
            {
                cover.setPeril(false);
            }

        }

    }

    private void setChains(List<BlockChain> unresolved)
    {
        foreach (BlockChain b in unresolved)
        {
            foreach (Transform t in b.getChain())
            {
                t.gameObject.GetComponent<Block>().setChained(true);
            }
        }
    }

    private int destroyChains()
    {
        int totalBreak = 0;

        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                if (gridArray[x, y] != null)
                    if (gridArray[x, y].GetComponent<Block>() != null)
                        if (gridArray[x, y].GetComponent<Block>().getChained() && gridArray[x, y].GetComponent<Block>().getDieNormal())
                        {
                            killRegularBlock(x, y);
                            totalBreak++;
                        }
            }
        }

        return totalBreak;
    }

    private void killRegularBlock(int x, int y)
    {
        gridArray[x, y].GetComponent<Block>().death();
    }

    public void spawnNextBlockGroup()
    {
        GameObject nextBlockGroup = (GameObject)Instantiate(Resources.Load("Prefabs/EndlessBlockGroup", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);

        GameObject b1 = (GameObject)Instantiate(Resources.Load(generateRandomBlock(), typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
        GameObject b2 = (GameObject)Instantiate(Resources.Load(generateRandomBlock(), typeof(GameObject)), new Vector2(4 + offset, 15), Quaternion.identity);

        b1.transform.parent = nextBlockGroup.transform;
        b2.transform.parent = nextBlockGroup.transform;
    }



    public GridCover getCover()
    {
        return cover;
    }



}
