﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndlessBlockGroup : MonoBehaviour {

    private float fall = 0;
    private float fallSpeed = 0.50f;

    private float fastfallSpeed;
    private float slowfallSpeed;
    const float dropRemainingSpeed = 0.025f;

    private float control = 0;
    const float controlSpeed = 0.075f;

    private bool semiEnabled;
    private bool afterMovement;
    private bool perfectLand;

    private AudioSource source;
    public AudioClip landing;
    public AudioClip rotate;

    private EndlessPlayer player;
    private EndlessTracker tracker;

    // Use this for initialization
    void Start()
    {
        perfectLand = true;
        semiEnabled = true;
        afterMovement = true;
        source = GetComponent<AudioSource>();
        player = FindObjectOfType<EndlessPlayer>();
        tracker = FindObjectOfType<EndlessTracker>();

        fastfallSpeed = tracker.getFastFallSpeed();
        slowfallSpeed = tracker.getSlowFallSpeed();



    }

    // Update is called once per frame
    void Update()
    {
        if (transform.childCount <= 0)
            Destroy(gameObject);


        if (semiEnabled)
        {
            if (Time.time - fall >= fallSpeed)
            {
                fall = Time.time;
                transform.position -= new Vector3(0, 1, 0);

                if (!isValidPosition())
                {
                    transform.position += new Vector3(0, 1, 0);
                    semiEnabled = false;
                    source.PlayOneShot(landing, 1.0f);
                }
                else
                {
                    player.updateGrid(this);
                }


            }

            checkUserInput();
        }
        else
        {
            if (Time.time - fall >= dropRemainingSpeed)
            {
                fall = Time.time;

                int count = 0;

                foreach (Transform t in transform)
                {
                    Vector2 pos = player.round(new Vector2(t.position.x, t.position.y - 1));

                    if (player.getTransformAtGridPositionOffset(pos) == null && t.position.y > 1)
                    {
                        t.position -= new Vector3(0, 1, 0);
                        pos = player.round(new Vector2(t.position.x, t.position.y - 1));
                        player.updateGrid(this);

                        perfectLand = false;
                        count++;
                    }




                }

                if (count <= 0 && afterMovement)
                {
                    if (!perfectLand)
                        source.PlayOneShot(landing, 1.0f);

                    afterMovement = false;
                    player.resolveCombos();
                }



            }



        }

    }

    void checkUserInput()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.position += new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position -= new Vector3(1, 0, 0);
            }
            else
                player.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("This method is happening");
            if(Time.time - control > controlSpeed)
            {
                transform.position += new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position -= new Vector3(1, 0, 0);
                }
                else
                    player.updateGrid(this);

                control = Time.time;
            }
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            transform.position -= new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position += new Vector3(1, 0, 0);
            }
            else
                player.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (Time.time - control > controlSpeed)
            {
                transform.position -= new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position += new Vector3(1, 0, 0);
                }
                else
                    player.updateGrid(this);

                control = Time.time;
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            fallSpeed = fastfallSpeed;
        }
        else
        {
            fallSpeed = slowfallSpeed;
        }



        if (Input.GetKeyDown(KeyCode.F))
        {
            transform.Rotate(0, 0, 90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, -90);
            }
            else
            {
                source.PlayOneShot(rotate);
                player.updateGrid(this);
            }
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            transform.Rotate(0, 0, -90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, 90);
            }
            else
            {
                source.PlayOneShot(rotate);
                player.updateGrid(this);
            }
        }

    }

    public bool isValidPosition()
    {
        foreach (Transform t in transform)
        {
            Vector2 pos = player.round(t.position);

            if (!player.isInsideGrid(pos))
            {
                return false;
            }

            if (player.getTransformAtGridPositionOffset(pos) != null && player.getTransformAtGridPositionOffset(pos).parent != transform)
            {
                return false;
            }


        }

        return true;
    }
}
