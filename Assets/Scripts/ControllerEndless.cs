﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerEndless : MonoBehaviour {

    public Transform pauseCanvas;
    public Transform gameOverCanvas;
    public Transform startUpMenu;

    public EndlessPlayer player;
    public EndlessTracker tracker;

    private AudioSource source;

    public AudioClip normal;
    public AudioClip danger;
    public AudioClip death;

    private bool inDanger;

    void Start ()
    {
        inDanger = false;
        source = GetComponent<AudioSource>();
        source.clip = normal;
        source.Play();
        startUpMenu.gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            pauseGame();
        }
        else if (player.getGameOver() && startUpMenu.gameObject.activeInHierarchy == false)
        {
            endGame();
        }

        if(player.getCover().getPeril() == true && inDanger == false)
        {
            source.Stop();
            source.clip = danger;
            source.Play();
            inDanger = true;
        }
        else if (player.getCover().getPeril() == false && inDanger == true)
        {
            source.Stop();
            source.clip = normal;
            source.Play();
            inDanger = false;
        }

    }

    public void pauseGame()
    {
        if (pauseCanvas.gameObject.activeInHierarchy == false)
        {
            pauseCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pauseCanvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void endGame()
    {
        if (gameOverCanvas.gameObject.activeInHierarchy == false)
        {
            source.PlayOneShot(death);
            gameOverCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void startNewGame(string mode)
    {
        tracker.setUp(mode);


        startUpMenu.gameObject.SetActive(false);      
        player.setGameOver(false);
        player.spawnNextBlockGroup();
        Time.timeScale = 1;
    }

    public void pullUpStartMenu()
    {
        gameOverCanvas.gameObject.SetActive(false);
        startUpMenu.gameObject.SetActive(true);
        clearBlocks();
        player.getCover().setPeril(false);
        player.stopRoutines();
    }

    public void clearBlocks()
    {
        Block[] blocks = FindObjectsOfType<Block>();

        foreach (Block b in blocks)
        {
            GameObject.Destroy(b.gameObject);
        }
    }
}
