﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class MenuManager : MonoBehaviour {

    // Use this for initialization
    public Image black;
    public Animator anim;

    private void Start()
    {
        
    }

    public void load(string sceneName)
    {
        Time.timeScale = 1;
        Debug.Log("Click");
        StartCoroutine(fading(sceneName));

    }

    IEnumerator fading(string sceneName)
    {
        anim.SetBool("fade", true);
        yield return new WaitUntil(() => black.color.a == 1);
        SceneManager.LoadScene(sceneName);
    }
}
