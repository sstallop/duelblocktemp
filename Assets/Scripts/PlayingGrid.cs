﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;

public class PlayingGrid : MonoBehaviour {

    private int garbage;
    public int player;

    private DeckItem nextBlock;
    protected Deck deck;

    protected bool gameOver;

    protected const int gridWidth = 8;
    protected const int gridHeight = 15;

    protected AudioSource source;
    protected AudioSource comboKing;

    public AudioClip chainBreak;
    public AudioClip minorDamage;
    public AudioClip majorDamage;

    public IEnumerator comboRoutine;

    protected Transform[,] gridArray = new Transform[gridWidth, gridHeight];
    protected float shieldTime;
    protected float burnTime;

    private float originalComboPitch;

    public int offset;
    public bool ai;

    public void Start()
    {
        deck = FindObjectOfType<DeckManager>().loadDeckForGame(player);
        deck.shuffle();
        nextBlock = deck.draw();

        shieldTime = -20;
        burnTime = -20;

        spawnNextBlockGroup();
        source = GetComponents<AudioSource>()[0];

        comboKing = GetComponents<AudioSource>()[1];
        originalComboPitch = comboKing.pitch;

        gameOver = false;
    }


    public Transform getTransformAtGridPositionOffset(Vector2 pos)
    {
        if (pos.y > gridHeight - 1)
        {
            return null;
        }
        else
        {
            return gridArray[(int)pos.x - offset, (int)pos.y];
        }
    }

    public string generateRandomBlock()
    {
        int ran = Random.Range(1, 5);
        string name = "";

        switch (ran)
        {
            case 1: name = "Prefabs/Red"; break;
            case 2: name = "Prefabs/Blue"; break;
            case 3: name = "Prefabs/Green"; break;
            case 4: name = "Prefabs/Yellow"; break;

            default: break;
        }

        return name;
    }

    protected List<BlockChain> findAllChains()
    {
        List<BlockChain> chains = new List<BlockChain>();

        for (int x = 1; x < gridWidth; x++)
        {
            for (int y = 1; y < gridHeight; y++)
            {


                if (gridArray[x, y] == null)
                    continue;

                Transform t = gridArray[x, y];

                bool inChain = false;

                foreach (BlockChain bChain in chains)
                {
                    if (bChain.contains(t))
                        inChain = true;
                }

                if (inChain)
                    continue;

                BlockChain newChain = findChain(t);

                if (newChain != null)
                    chains.Add(newChain);
            }
        }

        return chains;
    }

    protected BlockChain findChain(Transform t)
    {
        List<Transform> currentChain = new List<Transform> { t };
        List<Transform> nextBlocksToCheck = new List<Transform>() { t };

        while (nextBlocksToCheck.Count > 0)
        {
            Transform pi = nextBlocksToCheck[0];
            Transform nextInChain = findNextBlockInChain(pi, currentChain);

            while (nextInChain != null)
            {
                currentChain.Add(nextInChain);
                nextBlocksToCheck.Add(nextInChain);
                nextInChain = findNextBlockInChain(pi, currentChain);
            }



            nextBlocksToCheck.Remove(pi);
        }

        if (currentChain.Count >= 4)
            return new BlockChain(currentChain);
        else
            return null;
    }

    protected Transform findNextBlockInChain(Transform block, List<Transform> ignoredBlocks)
    {


        int x = (int)Mathf.Round(block.position.x);
        int y = (int)Mathf.Round(block.position.y);


        if (y < gridHeight - 1 && (ignoredBlocks.Count <= 0 || !ignoredBlocks.Contains(getTransformAtGridPositionOffset(new Vector2(x, y + 1)))))
        {

            Transform top = getTransformAtGridPositionOffset(new Vector2(x, y + 1));

            if (top != null && block != null)
                if (top.gameObject.GetComponent<Block>() != null && block.gameObject.GetComponent<Block>() != null)
                    if (block.gameObject.GetComponent<Block>().color.Equals(top.gameObject.GetComponent<Block>().color))
                        return top;

        }

        if (y > 0 && (ignoredBlocks.Count <= 0 || !ignoredBlocks.Contains(getTransformAtGridPositionOffset(new Vector2(x, y - 1)))))
        {
            Transform bottom = getTransformAtGridPositionOffset(new Vector2(x, y - 1));

            if (bottom != null && block != null)
                if (bottom.gameObject.GetComponent<Block>() != null && block.gameObject.GetComponent<Block>() != null)
                    if (block.gameObject.GetComponent<Block>().color.Equals(bottom.gameObject.GetComponent<Block>().color))
                        return bottom;
        }

        if (x < gridWidth - 1 + offset && (ignoredBlocks.Count <= 0 || !ignoredBlocks.Contains(getTransformAtGridPositionOffset(new Vector2(x + 1, y)))))
        {
            Transform right = getTransformAtGridPositionOffset(new Vector2(x + 1, y));

            if (right != null && block != null)
                if (right.gameObject.GetComponent<Block>() != null && block.gameObject.GetComponent<Block>() != null)
                    if (block.gameObject.GetComponent<Block>().color.Equals(right.gameObject.GetComponent<Block>().color))
                        return right;
        }

        if (x > offset && (ignoredBlocks.Count <= 0 || !ignoredBlocks.Contains(getTransformAtGridPositionOffset(new Vector2(x - 1, y)))))
        {
            Transform left = getTransformAtGridPositionOffset(new Vector2(x - 1, y));

            if (left != null && block != null)
                if (left.gameObject.GetComponent<Block>() != null && block.gameObject.GetComponent<Block>() != null)
                    if (block.gameObject.GetComponent<Block>().color.Equals(left.gameObject.GetComponent<Block>().color))
                        return left;
        }



        return null;
    }

    public bool isInsideGrid(Vector2 pos)
    {
        return ((int)pos.x > offset && (int)pos.x < gridWidth + offset && (int)pos.y > 0);
    }

    public Vector2 round(Vector2 pos)
    {
        return new Vector2(Mathf.Round(pos.x), Mathf.Round(pos.y));
    }

    public Vector3 truncate(Vector2 pos)
    {
        return new Vector2((int) pos.x, (int) pos.y);
    }

    public void addGarbage(int garbage)
    {
        this.garbage += garbage;
    }

    public void removeGarbage(int garbage)
    {
        this.garbage -= garbage;
    }


    public int getGarbage()
    {
        return garbage;
    }

    public bool getGameOver()
    {
        return gameOver;
    }

    public void setGameOver(bool gameOver)
    {
        this.gameOver = gameOver;
    }

    public float getShieldTime()
    {
        return shieldTime;
    }

    public void setShieldTime(float shieldTime)
    {
        this.shieldTime = shieldTime;
    }

    public float getBurnTime()
    {
        return burnTime;
    }

    public void setBurnTime(float burnTime)
    {
        this.burnTime = burnTime;
    }

    public void stopRoutines()
    {
        StopAllCoroutines();
    }

    public void clearGrid()
    {
        for(int i = 0; i < gridWidth; i++)
        {
            for(int j = 0; j < gridHeight; j++)
            {
                if (gridArray[i, j] != null)
                    GameObject.Destroy(gridArray[i, j].gameObject);

                
            }
        }

    }











    // HELPER METHODS
    // HELPER METHODS
    // HELPER METHODS
    // HELPER METHODS
    // HELPER METHODS
    // HELPER METHODS

    protected void unloadGarbage()
    {
        ArrayList columns = new ArrayList { 1, 2, 3, 4, 5, 6, 7 };
        int limit = 7;

        while (garbage > 0 && limit > 0)
        {
            int rowCount = 0;

            if (garbage >= 7)
            {
                spawnGarbageLine(rowCount);
                rowCount++;
                garbage -= 7;
                limit -= 7;
            }
            else
            {
                int ran = Random.Range(1, columns.Count + 1);
                spawnGarbageGroup(rowCount, (int)columns[ran - 1]);
                columns.RemoveAt((ran - 1));

                garbage--;
                limit--;

            }
        }

        if (garbage > 0)
            source.PlayOneShot(majorDamage);
        else
            source.PlayOneShot(minorDamage);

    }


    public void spawnNextBlockGroup()
    {
        GameObject nextBlockGroup = (GameObject)Instantiate(Resources.Load("Prefabs/BlankGroup", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
        GameObject b1;
        GameObject b2;
        GameObject eff1;
        GameObject eff2;

        string color1 = nextBlock.getB1();
        string color2 = nextBlock.getB2();
        string effect1 = nextBlock.getE1();
        string effect2 = nextBlock.getE2();



        switch (color1)
        {
            case "red":
                b1 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
                break;
            case "blue":
                b1 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
                break;
            case "green":
                b1 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
                break;
            case "yellow":
                b1 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(4 + offset, 14), Quaternion.identity);
                break;
            default:
                b1 = null;
                break;
        }
        switch (color2)
        {
            case "red":
                b2 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(4 + offset, 15), Quaternion.identity);
                break;
            case "blue":
                b2 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(4 + offset, 15), Quaternion.identity);
                break;
            case "green":
                b2 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(4 + offset, 15), Quaternion.identity);
                break;
            case "yellow":
                b2 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(4 + offset, 15), Quaternion.identity);
                break;
            default:
                b2 = null;
                break;
        }

        Vector3 effPosition1 = new Vector3(b1.transform.position.x, b1.transform.position.y, b1.transform.position.z - 2);
        Vector3 effPosition2 = new Vector3(b2.transform.position.x, b2.transform.position.y, b2.transform.position.z - 2);

        if (!effect1.Equals(""))
        {
            string modName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(effect1);
            eff1 = (GameObject)Instantiate(Resources.Load("Prefabs/" + modName, typeof(GameObject)), effPosition1, Quaternion.identity);
            eff1.transform.parent = b1.transform;
        }
        if (!effect2.Equals(""))
        {
            string modName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(effect2);
            eff2 = (GameObject)Instantiate(Resources.Load("Prefabs/" + modName, typeof(GameObject)), effPosition2, Quaternion.identity);
            eff2.transform.parent = b2.transform;
        }

        b1.transform.parent = nextBlockGroup.transform;
        b2.transform.parent = nextBlockGroup.transform;

        nextBlock = deck.draw();
    }




    protected void spawnGarbageGroup(int row, int col)
    {
        Instantiate(Resources.Load("Prefabs/GarbageFinal", typeof(GameObject)), new Vector2(col + offset, 17 + row), Quaternion.identity);
    }

    protected void spawnGarbageLine(int row)
    {
        for (int i = 1; i < 8; i++)
        {
            spawnGarbageGroup(row, i);
        }
    }

    protected void setChains(List<BlockChain> unresolved)
    {
        foreach (BlockChain b in unresolved)
        {
            foreach (Transform t in b.getChain())
            {
                t.gameObject.GetComponent<Block>().setChained(true);
            }
        }
    }

    protected void activatePriorityOneEffects()
    {
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                if (gridArray[x, y] != null)
                    if (gridArray[x, y].GetComponent<Block>() != null)
                        if (gridArray[x, y].GetComponent<Block>().getChained())
                            if (gridArray[x, y].transform.GetComponentInChildren<Effect>() != null)
                            {
                                switch (gridArray[x, y].transform.GetComponentInChildren<Effect>().getName())
                                {
                                    case "Bomb(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateBomb(gridArray, x, y, gridWidth, gridHeight);
                                        break;
                                    case "Spark(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateSpark(gridArray, x, y, offset, gridWidth, gridHeight);
                                        break;
                                    default: break;
                                }
                            }


            }
        }
    }

    protected void activatePriorityTwoEffects()
    {
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                if (gridArray[x, y] != null)
                    if (gridArray[x, y].GetComponent<Block>() != null)
                        if (gridArray[x, y].GetComponent<Block>().getChained())
                            if (gridArray[x, y].transform.GetComponentInChildren<Effect>() != null && gridArray[x, y].GetComponent<Block>().getDieNormal())
                            {
                                switch (gridArray[x, y].transform.GetComponentInChildren<Effect>().getName())
                                {
                                    case "Shield(Clone)":
                                        shieldTime = Time.time;
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateShield(offset, gridWidth, gridHeight);
                                        break;
                                    case "Attack(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateAttack(player);
                                        break;
                                    case "Kamikaze(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateKamikaze(player);
                                        break;
                                    case "Burn(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateBurn(player);
                                        break;
                                    case "Healthpack(Clone)":
                                        gridArray[x, y].transform.GetComponentInChildren<Effect>().activateHeal(player);
                                        break;
                                    default: break;
                                }
                            }


            }
        }
    }

    protected int destroyChains()
    {
        int totalBreak = 0;

        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                if (gridArray[x, y] != null)
                    if (gridArray[x, y].GetComponent<Block>() != null)
                        if (gridArray[x, y].GetComponent<Block>().getChained() && gridArray[x, y].GetComponent<Block>().getDieNormal())
                        {
                            killRegularBlock(x, y);
                            totalBreak++;

                            removeSurroundingGarbage(x, y);
                        }
            }
        }

        return totalBreak;
    }

    protected void killRegularBlock(int x, int y)
    {
        gridArray[x, y].GetComponent<Block>().death();
    }


    protected void removeSurroundingGarbage(int x, int y)
    {
        if (y + 1 < gridHeight)
            if (gridArray[x, y + 1] != null)
            {
                if (gridArray[x, y + 1].gameObject.GetComponent<GarbageFinal>() != null && gridArray[x, y + 1].gameObject.GetComponent<GarbageFinal>().getDieNormal())
                {
                    gridArray[x, y + 1].gameObject.GetComponent<GarbageFinal>().death();
                }

            }

        if (y - 1 > 0)
            if (gridArray[x, y - 1] != null)
            {
                if (gridArray[x, y - 1].gameObject.GetComponent<GarbageFinal>() != null && gridArray[x, y - 1].gameObject.GetComponent<GarbageFinal>().getDieNormal())
                {
                    gridArray[x, y - 1].gameObject.GetComponent<GarbageFinal>().death();
                }

            }

        if (x + 1 < gridWidth)
            if (gridArray[x + 1, y] != null)
            {
                if (gridArray[x + 1, y].gameObject.GetComponent<GarbageFinal>() != null && gridArray[x + 1, y].gameObject.GetComponent<GarbageFinal>().getDieNormal())
                {
                    gridArray[x + 1, y].gameObject.GetComponent<GarbageFinal>().death();
                }

            }

        if (x - 1 > 0)
            if (gridArray[x - 1, y] != null)
            {
                if (gridArray[x - 1, y].gameObject.GetComponent<GarbageFinal>() != null && gridArray[x - 1, y].gameObject.GetComponent<GarbageFinal>().getDieNormal())
                {
                    gridArray[x - 1, y].gameObject.GetComponent<GarbageFinal>().death();
                }

            }
    }

    public void updateGrid(BlockGroupFinal blockGroup)
    {
        for (int y = 1; y < gridHeight; y++)
        {
            for (int x = 1; x < gridWidth; x++)
            {
                if (gridArray[x, y] != null)
                {
                    if (gridArray[x, y].parent == blockGroup.transform)
                    {
                        gridArray[x, y] = null;
                    }
                }

            }
        }

        foreach (Transform t in blockGroup.transform)
        {
            Vector2 pos = round(t.position);

            if (pos.y < gridHeight)
            {
                gridArray[(int)pos.x - offset, (int)pos.y] = t;
            }
        }
    }

    public void updateGrid(GarbageFinal garbageGroup)
    {
        for (int y = 0; y < gridHeight; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                if (gridArray[x, y] != null)
                {
                    if (gridArray[x, y] == garbageGroup.transform)
                    {
                        gridArray[x, y] = null;
                    }
                }

            }
        }

        Transform t = garbageGroup.transform;
        Vector2 pos = round(t.position);

        if (pos.y < gridHeight)
        {
            gridArray[(int)pos.x - offset, (int)pos.y] = t;
        }

    }


    //LAST AWKWARD BIT
    //LAST AWKWARD BIT
    //LAST AWKWARD BIT
    //LAST AWKWARD BIT
    //LAST AWKWARD BIT

    public void resolveCombos()
    {
        comboRoutine = comboFix();
        StartCoroutine(comboRoutine);
    }



    protected IEnumerator comboFix()
    {
        List<BlockChain> unresolved = findAllChains();

        int combo = 0;

        while (unresolved.Count > 0)
        {
            combo++;

            Vector3 effPosition = new Vector3(4 + offset, 12, transform.position.z - 5);
            GameObject comboObject = (GameObject)Instantiate(Resources.Load("ParticleClusters/ComboEffect", typeof(GameObject)), effPosition, Quaternion.identity);
            comboObject.GetComponentInChildren<TextMeshPro>().text = combo + " combo!";

            comboKing.pitch = originalComboPitch + (0.1f * (combo - 1));
            comboKing.PlayOneShot(chainBreak);
           

            int totalBreak = 0;

            setChains(unresolved);
            activatePriorityOneEffects();
            activatePriorityTwoEffects();
            totalBreak += destroyChains();

            int score = (int)(totalBreak * 25 * Mathf.Pow(3, combo - 1) + 200 * (combo - 1));

            if(player == 1)
                FindObjectOfType<ScoreBoard>().setP1Score(FindObjectOfType<ScoreBoard>().getP1Score() + score);
            else if(player == 2)
                FindObjectOfType<ScoreBoard>().setP2Score(FindObjectOfType<ScoreBoard>().getP2Score() + score);

            yield return new WaitForSeconds(1.25f);
            unresolved = findAllChains();
        }

        if (gridArray[4, 14] != null)
        {
            gameOver = true;
        }
        if (!gameOver)
        {
            if (Time.time - burnTime < 10)
            {
                addGarbage(1);
            }
            if (garbage > 0 && Time.time - shieldTime > 10)
            {
                unloadGarbage();
                yield return new WaitForSeconds(1f);
            }

            //Debug.Log("Still running in the coroutine :(");
            spawnNextBlockGroup();
        }

    }


    public string toString()
    {
        string result = "blocks at: ";

        for(int i = 0; i < gridWidth; i++)
        {
            for(int j = 0; j < gridHeight; j++)
            {
                if (gridArray[i,j] != null)
                    result += "(" + i + "," + j + "), ";
            }
        }

        return result;
    }

}
