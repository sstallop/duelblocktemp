﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControllerDeckEdit : MonoBehaviour {

    public Canvas first;
    public Canvas second;

    private DeckManager manager;
	// Use this for initialization
	void Start ()
    {
        manager = FindObjectOfType<DeckManager>();

        first.gameObject.SetActive(true);
        second.gameObject.SetActive(false);
	}

    void Update()
    {
    }

    public void startDeckEdit(string player)
    {
        manager.setPlayer(player);        
        second.gameObject.SetActive(true);
        first.gameObject.SetActive(false);
    }
	
	public void pullUpMenu()
    {
        manager.clearDeckItem();
        manager.newDeck();
        FindObjectOfType<Dropdown>().ClearOptions();
        second.gameObject.SetActive(false);
        first.gameObject.SetActive(true);
    }

}
