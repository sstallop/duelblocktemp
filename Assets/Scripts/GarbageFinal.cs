﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageFinal : MonoBehaviour
{

    private int player;
    private float fall = 0;

    const float fallSpeed = 0.035f;
    const float dropRemainingSpeed = 0.05f;

    private bool semiEnabled;
    private bool afterMovement;
    private bool dieNormal;

    private Animation anim;

    private ParticleSystem pSystem;

    PlayingGrid p1;
    PlayingGrid p2;


    void Start()
    {
        dieNormal = true;
        semiEnabled = true;
        pSystem = GetComponent<ParticleSystem>();
        anim = GetComponent<Animation>();
        p1 = GameObject.Find("GridP1").GetComponent<PlayingGrid>();
        p2 = GameObject.Find("GridP2").GetComponent<PlayingGrid>();

        if (transform.position.x < 18)
        {
            player = 1;
        }
        else
        {
            player = 2;
        }
    }


    void Update()
    {
        if (semiEnabled)
        {

            if (Time.time - fall >= fallSpeed)
            {
                fall = Time.time;
                transform.position -= new Vector3(0, 1, 0);

                if (!isValidPosition())
                {
                    transform.position += new Vector3(0, 1, 0);
                    semiEnabled = false;
                }
                else
                {
                    if (player == 1)
                        p1.updateGrid(this);
                    else if (player == 2)
                        p2.updateGrid(this);
                }
                    

            }
        }
        else
        {
            if (Time.time - fall >= dropRemainingSpeed)
            {
                fall = Time.time;

                Transform t = transform;

                if(player == 1)
                {
                    Vector2 pos = p1.round(new Vector2(t.position.x, t.position.y - 1));

                    if (p1.getTransformAtGridPositionOffset(pos) == null && t.position.y > 1)
                    {
                        t.position -= new Vector3(0, 1, 0);
                        pos = p1.round(new Vector2(t.position.x, t.position.y - 1));
                        p1.updateGrid(this);

                    }
                }
                else if(player == 2)
                {
                    Vector2 pos = p2.round(new Vector2(t.position.x, t.position.y - 1));

                    if (p2.getTransformAtGridPositionOffset(pos) == null && t.position.y > 1)
                    {
                        t.position -= new Vector3(0, 1, 0);
                        pos = p2.round(new Vector2(t.position.x, t.position.y - 1));
                        p2.updateGrid(this);

                    }
                }


            }



        }


    }

    public bool isValidPosition()
    {
        Transform t = transform;

        if(player == 1)
        {
            Vector2 pos = p1.round(t.position);

            if (!p1.isInsideGrid(pos))
            {
                return false;
            }

            if (p1.getTransformAtGridPositionOffset(pos) != null && p1.getTransformAtGridPositionOffset(pos).parent != transform)
            {
                return false;
            }



            return true;
        }
        else if(player == 2)
        {
            Vector2 pos = p2.round(t.position);

            if (!p2.isInsideGrid(pos))
            {
                return false;
            }

            if (p2.getTransformAtGridPositionOffset(pos) != null && p2.getTransformAtGridPositionOffset(pos).parent != transform)
            {
                return false;
            }



            return true;
        }

        return true;
    }

    public void setDieNormal(bool dieNormal)
    {
        this.dieNormal = dieNormal;
    }

    public bool getDieNormal()
    {
        return dieNormal;
    }

    public void death()
    {
        pSystem.Play();

        anim.Play("death");

        if (!pSystem.loop)
        {
            Destroy(gameObject, pSystem.duration);
        }

    }

    public void explode()
    {
        anim.Play("explode");
        Destroy(gameObject, 0.5f);
    }
}

