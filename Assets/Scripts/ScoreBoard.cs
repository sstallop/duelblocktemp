﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreBoard : MonoBehaviour {


    private int p1Score;
    private int p2Score;

    private TextMeshPro mesh1;
    private TextMeshPro mesh2;

    private TextMeshPro garbage1;
    private TextMeshPro garbage2;

    int garbageTotal1;
    int garbageTotal2;

    public PlayingGrid p1;
    public PlayingGrid p2;

	void Start ()
    {
        p1Score = 0;
        p2Score = 0;

        garbageTotal1 = 0;
        garbageTotal2 = 0;

        mesh1 = transform.Find("ScoreTextP1").gameObject.GetComponent<TextMeshPro>();
        mesh2 = transform.Find("ScoreTextP2").gameObject.GetComponent<TextMeshPro>();
        garbage1 = transform.Find("GarbageTextP1").gameObject.GetComponent<TextMeshPro>();
        garbage2 = transform.Find("GarbageTextP2").gameObject.GetComponent<TextMeshPro>();

    }
	
	void Update ()
    {
        mesh1.text = "P1: " +  p1Score;
        mesh2.text = "P2: " +  p2Score;


        garbageTotal1 = p1.getGarbage();
        garbageTotal2 = p2.getGarbage();

        garbage1.text = garbageTotal1 + "";
        garbage2.text = garbageTotal2 + "";

        if(garbageTotal1 > 21)
        {
            garbage1.color = Color.red;
            
        }
        else if(garbageTotal1 > 7)
        {
            garbage1.color = Color.yellow;
           
        }
        else
        {
            garbage1.color = Color.cyan;
           
        }

        if (garbageTotal2 > 21)
        {
            garbage2.color = Color.red;
           
        }
        else if (garbageTotal2 > 7)
        {
            garbage2.color = Color.yellow;
            
        }
        else
        {
            garbage2.color = Color.cyan;
            
        }

    }

    public void setP1Score(int score)
    {
        int difference = score - p1Score;

        int garbage = difference / 100;
        int playerGarbage = p1.getGarbage();
        int garbageDifference =  playerGarbage - garbage;

        if (garbageDifference >= 0)
            p1.removeGarbage(garbage);
        else
        {
            p1.removeGarbage(playerGarbage);
            p2.addGarbage(-garbageDifference);
            garbage2.gameObject.GetComponent<Animation>().Play();
        }

        p1Score = score;
        

    }

    public int getP1Score()
    {
        return p1Score;
    }

    public void setP2Score(int score)
    {
        int difference = score - p2Score;

        int garbage = difference / 100;
        int playerGarbage = p2.getGarbage();
        int garbageDifference = playerGarbage - garbage;

        if (garbageDifference >= 0)
            p2.removeGarbage(garbage);
        else
        {
            p2.removeGarbage(playerGarbage);
            p1.addGarbage(-garbageDifference);
            garbage1.gameObject.GetComponent<Animation>().Play();
        }

        p2Score = score;
    }

    public int getP2Score()
    {
        return p2Score;
    }


}
