﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockGroupFinal : MonoBehaviour {

    private int player;
    private bool stopMotion;
    private float fall = 0;
    private float fallSpeed = 0.50f;

    const float fastfallSpeed = 0.025f;
    const float slowfallSpeed = 0.5f;
    const float dropRemainingSpeed = 0.025f;

    private float control = 0;
    const float controlSpeed = 0.075f;

    private bool semiEnabled;
    private bool afterMovement;
    private bool perfectLand;

    private AudioSource source;
    public AudioClip landing;
    public AudioClip rotate;

    PlayingGrid p1;
    PlayingGrid p2;


    // Use this for initialization
    void Start()
    {
        stopMotion = false;
        perfectLand = true;
        semiEnabled = true;
        afterMovement = true;
        source = GetComponent<AudioSource>();

        p1 = GameObject.Find("GridP1").GetComponent<PlayingGrid>();
        p2 = GameObject.Find("GridP2").GetComponent<PlayingGrid>();


        if(transform.position.x < p2.offset)
        {
            player = 1;
        }
        else
        {
            player = 2;
        }
    }

    // Update is called once per frame
    void Update()
    {
        checkGameOver();

        if (transform.childCount <= 0)
            Destroy(gameObject);

        if (!stopMotion)
        {
            if (semiEnabled)
            {
                movementA();
            }
            else
            {
                movementB();
            }
        }




    }

    void checkGameOver()
    {
        if (p1.getGameOver() == true || p2.getGameOver() == true)
        {
            if (((p1.getGameOver() == true && player == 1) || (p2.getGameOver() == true && player == 2)) && stopMotion == false)
            {
                foreach (Transform t in transform)
                {
                    StartCoroutine(gameLostMovement());
                }            
            }
            stopMotion = true;
        }
    }

    IEnumerator gameLostMovement()
    {
        foreach (Transform t in transform)
        {
            float randomAngle = Random.Range(0, 2* Mathf.PI);
            Vector3 direction = new Vector3(10 * Mathf.Pow(Mathf.Cos(randomAngle), 1), -10, -10);
            t.gameObject.GetComponent<Rigidbody>().velocity = direction;
        }

        yield return new WaitForSeconds(1f);

        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }
        
    }

    void movementA()
    {
        if (Time.time - fall >= fallSpeed)
        {
            fall = Time.time;
            transform.position -= new Vector3(0, 1, 0);

            if (!isValidPosition())
            {
                transform.position += new Vector3(0, 1, 0);
                semiEnabled = false;
                source.PlayOneShot(landing, 1.0f);
            }
            else
            {
                if (player == 1)
                    p1.updateGrid(this);
                else if (player == 2)
                    p2.updateGrid(this);
            }


        }
        if (player == 1)
            checkUserInputP1();
        else if (player == 2)
            checkUserInputP2();
    }

    void movementB()
    {
        if (Time.time - fall >= dropRemainingSpeed)
        {
            fall = Time.time;

            int count = 0;

            if (player == 1)
            {
                foreach (Transform t in transform)
                {
                    Vector2 pos = p1.round(new Vector2(t.position.x, t.position.y - 1));

                    if (p1.getTransformAtGridPositionOffset(pos) == null && t.position.y > 1)
                    {
                        t.position -= new Vector3(0, 1, 0);
                        pos = p1.round(new Vector2(t.position.x, t.position.y - 1));
                        p1.updateGrid(this);

                        perfectLand = false;
                        count++;
                    }


                }

                if (count <= 0 && afterMovement)
                {
                    if (!perfectLand)
                        source.PlayOneShot(landing, 1.0f);

                    afterMovement = false;
                    p1.resolveCombos();
                }
            }
            else if (player == 2)
            {
                foreach (Transform t in transform)
                {
                    Vector2 pos = p2.round(new Vector2(t.position.x, t.position.y - 1));

                    if (p2.getTransformAtGridPositionOffset(pos) == null && t.position.y > 1)
                    {
                        t.position -= new Vector3(0, 1, 0);
                        pos = p2.round(new Vector2(t.position.x, t.position.y - 1));
                        p2.updateGrid(this);

                        perfectLand = false;
                        count++;
                    }


                }

                if (count <= 0 && afterMovement)
                {
                    if (!perfectLand)
                        source.PlayOneShot(landing, 1.0f);

                    afterMovement = false;
                    p2.resolveCombos();
                }
            }


        }
    }

    


    void checkUserInputP1()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            transform.position += new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position -= new Vector3(1, 0, 0);
            }
            else
                p1.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            if (Time.time - control > controlSpeed)
            {
                transform.position += new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position -= new Vector3(1, 0, 0);
                }
                else
                    p1.updateGrid(this);

                control = Time.time;
            }
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            transform.position -= new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position += new Vector3(1, 0, 0);
            }
            else
                p1.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (Time.time - control > controlSpeed)
            {
                transform.position -= new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position += new Vector3(1, 0, 0);
                }
                else
                    p1.updateGrid(this);

                control = Time.time;
            }
        }

        if (Input.GetKey(KeyCode.S))
        {
            fallSpeed = fastfallSpeed;
        }
        else
        {
            fallSpeed = slowfallSpeed;
        }



        if (Input.GetKeyDown(KeyCode.F))
        {
            transform.Rotate(0, 0, 90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, -90);
            }
            else
            {
                source.PlayOneShot(rotate);
                p1.updateGrid(this);
            }
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            transform.Rotate(0, 0, -90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, 90);
            }
            else
            {
                source.PlayOneShot(rotate);
                p1.updateGrid(this);
            }
        }


    }

    void checkUserInputP2()
    {

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            transform.position += new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position -= new Vector3(1, 0, 0);
            }
            else
                p2.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            if (Time.time - control > controlSpeed)
            {
                transform.position += new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position -= new Vector3(1, 0, 0);
                }
                else
                    p2.updateGrid(this);

                control = Time.time;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            transform.position -= new Vector3(1, 0, 0);

            if (!isValidPosition())
            {
                transform.position += new Vector3(1, 0, 0);
            }
            else
                p2.updateGrid(this);

            control = Time.time;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (Time.time - control > controlSpeed)
            {
                transform.position -= new Vector3(1, 0, 0);

                if (!isValidPosition())
                {
                    transform.position += new Vector3(1, 0, 0);
                }
                else
                    p2.updateGrid(this);

                control = Time.time;
            }
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            fallSpeed = fastfallSpeed;
        }
        else
        {
            fallSpeed = slowfallSpeed;
        }



        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            transform.Rotate(0, 0, 90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, -90);
            }
            else
            {
                source.PlayOneShot(rotate);
                p2.updateGrid(this);
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightBracket))
        {
            transform.Rotate(0, 0, -90);

            if (!isValidPosition())
            {
                transform.Rotate(0, 0, 90);
            }
            else
            {
                source.PlayOneShot(rotate);
                p2.updateGrid(this);
            }
        }


    }

    public bool isValidPosition()
    {
        if(player == 1)
        {
            foreach (Transform t in transform)
            {
                Vector2 pos = p1.round(t.position);

                if (!p1.isInsideGrid(pos))
                {
                    return false;
                }

                if (p1.getTransformAtGridPositionOffset(pos) != null && p1.getTransformAtGridPositionOffset(pos).parent != transform)
                {
                    return false;
                }


            }

            return true;
        }
        else if(player == 2)
        {
            foreach (Transform t in transform)
            {
                Vector2 pos = p2.round(t.position);

                if (!p2.isInsideGrid(pos))
                {
                    return false;
                }

                if (p2.getTransformAtGridPositionOffset(pos) != null && p2.getTransformAtGridPositionOffset(pos).parent != transform)
                {
                    return false;
                }


            }

            return true;
        }

        return true;

    }
}
