﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockChain {

    private List<Transform> chain;

    public BlockChain()
    {
        chain = new List<Transform>();
    }

    public BlockChain(List<Transform> chain)
    {
        this.chain = chain;
    }

    public bool contains(Transform t)
    {
        return chain.Contains(t);
    }

	public string toString()
    {
        return chain.ToString();
    }

    public List<Transform> getChain()
    {
        return chain;
    }
}
