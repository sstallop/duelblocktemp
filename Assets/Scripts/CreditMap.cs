﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditMap : MonoBehaviour {

    private Text text;

    void Start ()
    {
        text = gameObject.GetComponent<Text>();

    }
	
	
	void Update ()
    {
        text.text = "Credits Left " + FindObjectOfType<DeckManager>().getDeck().getCreditsLeft();
	}
}
