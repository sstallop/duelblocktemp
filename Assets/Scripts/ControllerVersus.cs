﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerVersus : MonoBehaviour {

    public Transform pauseCanvas;
    public Transform gameOverCanvas;

    public PlayingGrid p1;
    public PlayingGrid p2;
    public ScoreBoard scoreBoard;

    private AudioSource source;
    public AudioClip death;

    private bool isPlaying;

    void Start ()
    {
        source = GetComponent<AudioSource>();
        isPlaying = true;
    }
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isPlaying)
        {
            pauseGame();
        }
        else if((p1.getGameOver() || p2.getGameOver()) && isPlaying)
        {
            isPlaying = false;
            StartCoroutine(endGame());
        }

    }

    public void pauseGame()
    {
        if(pauseCanvas.gameObject.activeInHierarchy == false)
        {
            pauseCanvas.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pauseCanvas.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public IEnumerator endGame()
    {
        
            if (p1.getGameOver())
            {
                Instantiate(Resources.Load("Prefabs/LoseText", typeof(GameObject)), new Vector3(4 + p1.offset, 6, -7), Quaternion.identity);
                Instantiate(Resources.Load("Prefabs/WinText", typeof(GameObject)), new Vector3(4 + p2.offset, 10, -7), Quaternion.identity);

            }
            else if (p2.getGameOver())
            {
                Instantiate(Resources.Load("Prefabs/LoseText", typeof(GameObject)), new Vector3(4 + p2.offset, 6, -7), Quaternion.identity);
                Instantiate(Resources.Load("Prefabs/WinText", typeof(GameObject)), new Vector3(4 + p1.offset, 10, -7), Quaternion.identity);
            }

            source.PlayOneShot(death, 1f);

      
            yield return new WaitForSeconds(2);
            gameOverCanvas.gameObject.SetActive(true);

    }

    public void startNewGame()
    {
        gameOverCanvas.gameObject.SetActive(false);
        clearBlocks();
        scoreBoard.setP1Score(0);
        scoreBoard.setP2Score(0);
        p1.removeGarbage(p1.getGarbage());
        p2.removeGarbage(p2.getGarbage());
        p1.setShieldTime(-20f);
        p2.setShieldTime(-20f);
        p1.setBurnTime(-20f);
        p2.setBurnTime(-20f);
        p1.setGameOver(false);
        p2.setGameOver(false);
        p1.spawnNextBlockGroup();
        p2.spawnNextBlockGroup();
        p1.stopRoutines();
        p2.stopRoutines();
        isPlaying = true;
    }



    public void clearBlocks()
    {
        Block[] blocks = FindObjectsOfType<Block>();
        //Effect[] effects = FindObjectsOfType<Effect>();
        GarbageFinal[] garbage = FindObjectsOfType<GarbageFinal>();
        ParticleSystem[] pSystems = FindObjectsOfType<ParticleSystem>();
        ComboScript[] cScripts = FindObjectsOfType<ComboScript>();

        foreach(Block b in blocks)
        {
            Destroy(b.gameObject);
        }

        foreach(ParticleSystem pSystem in pSystems)
        {
            Destroy(pSystem.gameObject);
        }

        foreach (GarbageFinal g in garbage)
        {
            Destroy(g.gameObject);
        }

        foreach (ComboScript c in cScripts)
        {
            Destroy(c.gameObject);
        }

    }
}
