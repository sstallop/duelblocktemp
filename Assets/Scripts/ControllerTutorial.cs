﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerTutorial : MonoBehaviour {

    private Canvas currentScreen;
    private int currentIndex;

    public Canvas s1;
    public Canvas s2;
    public Canvas s3;
    public Canvas s4;
    public Canvas s5;
    public Canvas s6;
    public Canvas s7;


    void Start()
    {
        s1.gameObject.SetActive(true);
        s2.gameObject.SetActive(false);
        s3.gameObject.SetActive(false);
        s4.gameObject.SetActive(false);
        s5.gameObject.SetActive(false);
        s6.gameObject.SetActive(false);
        s7.gameObject.SetActive(false);
        currentIndex = 1;
    }

    public void next()
    {
        if(currentIndex < 7)
        currentIndex++;

        switch (currentIndex)
        {
            case 2:
                s1.gameObject.SetActive(false);
                s2.gameObject.SetActive(true);
                break;
            case 3:
                s2.gameObject.SetActive(false);
                s3.gameObject.SetActive(true);
                break;
            case 4:
                s3.gameObject.SetActive(false);
                s4.gameObject.SetActive(true);
                break;
            case 5:
                s4.gameObject.SetActive(false);
                s5.gameObject.SetActive(true);
                break;
            case 6:
                s5.gameObject.SetActive(false);
                s6.gameObject.SetActive(true);
                break;
            case 7:
                s6.gameObject.SetActive(false);
                s7.gameObject.SetActive(true);
                break;
        }

    }

    public void previous()
    {
        if (currentIndex > 1)
            currentIndex--;

        switch (currentIndex)
        {
            case 1:
                s2.gameObject.SetActive(false);
                s1.gameObject.SetActive(true);
                break;
            case 2:
                s3.gameObject.SetActive(false);
                s2.gameObject.SetActive(true);
                break;
            case 3:
                s4.gameObject.SetActive(false);
                s3.gameObject.SetActive(true);
                break;
            case 4:
                s5.gameObject.SetActive(false);
                s4.gameObject.SetActive(true);
                break;
            case 5:
                s6.gameObject.SetActive(false);
                s5.gameObject.SetActive(true);
                break;
            case 6:
                s7.gameObject.SetActive(false);
                s6.gameObject.SetActive(true);
                break;
        }
    }

}
