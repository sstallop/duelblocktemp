﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeckDropDown : MonoBehaviour
{
    private Dropdown dropDown;

    void Start()
    {
        dropDown = GetComponent<Dropdown>();
    }

    void Update()
    {
        
    }

    public void loadDeckItemAt(int index)
    {
        GameObject.Destroy(FindObjectOfType<DeckManager>().getSample1());
        GameObject.Destroy(FindObjectOfType<DeckManager>().getSample2());
        GameObject.Destroy(FindObjectOfType<DeckManager>().getSampleEffect1());
        GameObject.Destroy(FindObjectOfType<DeckManager>().getSampleEffect2());

        FindObjectOfType<DeckManager>().setOutsideB1(FindObjectOfType<DeckManager>().getDeck().get(index).getB1());
        FindObjectOfType<DeckManager>().setOutsideB2(FindObjectOfType<DeckManager>().getDeck().get(index).getB2());
        FindObjectOfType<DeckManager>().setOutsideE1(FindObjectOfType<DeckManager>().getDeck().get(index).getE1());
        FindObjectOfType<DeckManager>().setOutsideE2(FindObjectOfType<DeckManager>().getDeck().get(index).getE2());

        FindObjectOfType<DeckManager>().loadDeckItem();
    }

    public void add(DeckItem deckItem)
    {

        Dropdown.OptionData item = new Dropdown.OptionData(deckItem.ToString());
        dropDown.options.Add(item);
        

    }

    public void remove(DeckItem deckItem)
    {
        Dropdown.OptionData convertedItem = new Dropdown.OptionData(deckItem.getB1() + "," + deckItem.getE1() + "," + deckItem.getB2() + "," + deckItem.getE2());

        for(int i = 0; i < GetComponent<Dropdown>().options.Count; i++) 
        {
            if (dropDown.options[i].text.Equals(convertedItem.text))
            {
                dropDown.options.RemoveAt(i);
                return;
            }
        }

    }


}
