﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckSlot : MonoBehaviour {

    public int index;
    private DeckItem dItem;
    private GameObject sample1;
    private GameObject sample2;
 
	
	public void displayBlock(Deck deck)
    {
        dItem = deck.get(index);

        string color1 = dItem.getB1();

        if (color1.Equals("red"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(-4, transform.position.y), Quaternion.identity);
        else if (color1.Equals("blue"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(-4, transform.position.y), Quaternion.identity);
        else if (color1.Equals("green"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(-4, transform.position.y), Quaternion.identity);
        else if (color1.Equals("yellow"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(-4, transform.position.y), Quaternion.identity);


        string color2 = dItem.getB2();

        if (color2.Equals("red"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(transform.position.x + 1, transform.position.y), Quaternion.identity);
        else if (color2.Equals("blue"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(transform.position.x + 1, transform.position.y), Quaternion.identity);
        else if (color2.Equals("green"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(transform.position.x + 1, transform.position.y), Quaternion.identity);
        else if (color2.Equals("yellow"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(transform.position.x + 1, transform.position.y), Quaternion.identity);

    }


}
