﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DeckItem : Object {

    private string b1;
    private string b2;
    private string e1;
    private string e2;

    public DeckItem()
    {
        b1 = "";
        b2 = "";
        e1 = "";
        e2 = "";
    }

    public DeckItem(string b1, string b2)
    {
        this.b1 = b1;
        this.b2 = b2;
        e1 = "";
        e2 = "";
    }

    public DeckItem(string b1, string b2, string e1, string e2)
    {
        this.b1 = b1;
        this.b2 = b2;
        this.e1 = e1;
        this.e2 = e2;
    }

    public void setB1(string b1)
    {
        this.b1 = b1;
    }

    public void setB2(string b2)
    {
        this.b2 = b2;
    }

    public void setE1(string e1)
    {
        this.e1 = e1;
    }

    public void setE2(string e2)
    {
        this.e2 = e2;
    }

    public void clear()
    {
        b1 = "";
        b2 = "";
        e1 = "";
        e2 = "";
    }

    public string getB1()
    {
        return b1;
    }

    public string getB2()
    {
        return b2;
    }

    public string getE1()
    {
        return e1;
    }

    public string getE2()
    {
        return e2;
    }

    override
    public string ToString()
    {
        return b1 + "," + e1 + "," + b2 + "," + e2;
    }


	
}
