﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableTile : MonoBehaviour {

    public string title;
    public string creditCost;
    private string description;

    private GameObject boxHolder;

    void Start()
    {
        loadText(title);
    }

    void loadText(string title)
    {
        TextAsset effectStuff = Resources.Load(title) as TextAsset;
        description = effectStuff.text;
    }


    public void updateDescriptionBox()
    {
        boxHolder = (GameObject)(Instantiate(Resources.Load("Prefabs/descriptionBox", typeof(GameObject)), new Vector3(transform.position.x - 110, transform.position.y - 45, -5), Quaternion.identity));
        boxHolder.transform.parent = GameObject.Find("DeckEditor").transform;
        boxHolder.transform.localScale = new Vector3(1, 1, 1);

        DescriptionBox box = boxHolder.GetComponent<DescriptionBox>();
        
        box.setName(title);
        box.setCost(creditCost + " credits required");
        box.setDescription(description);
    }

    public void destroyBox()
    {
        Destroy(boxHolder);
    }
}
