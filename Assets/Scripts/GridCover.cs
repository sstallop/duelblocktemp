﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCover : MonoBehaviour {


    private Animator anim;

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void setPeril(bool condition)
    {
        anim.SetBool("inPeril", condition);
    }

    public bool getPeril()
    {
        return anim.GetBool("inPeril");
    }

}
