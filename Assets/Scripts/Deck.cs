﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Deck {

    const int creditsTotal = 100;
    private int creditsLeft;

    private int minSize;
    private int maxSize;
    private int currentItem;

    private bool legal;
    private string legalText;

	private List<DeckItem> deck;
    private Dictionary<string, int> colors;
    private Dictionary<string, int> map;
    
	public Deck()
    {
        legal = false;
        legalText = "no deck present";

        map = new Dictionary<string, int>();

        map.Add("bomb", 10);
        map.Add("spark", 10);
        map.Add("shield", 50);
        map.Add("kamikaze", 20);
        map.Add("attack", 30);
        map.Add("healthpack", 20);
        map.Add("burn", 40);


        currentItem = 0;

        minSize = 20;
        maxSize = 50;

        creditsLeft = creditsTotal;


        deck = new List<DeckItem>();

        colors = new Dictionary<string, int>();

        colors.Add("red", 0);
        colors.Add("green", 0);
        colors.Add("blue", 0);
        colors.Add("yellow", 0);

    }

    public void add(DeckItem deckItem)
    {
        if (deckItem.getE1() != null && !deckItem.getE1().Equals(""))
            creditsLeft -= map[deckItem.getE1()];

        if(deckItem.getE2() != null && !deckItem.getE2().Equals(""))
            creditsLeft -= map[deckItem.getE2()];


        deck.Add(deckItem);

        string b1 = deckItem.getB1();
        string b2 = deckItem.getB2();

        colors[b1]++;
        colors[b2]++;
    }

    public void remove(DeckItem deckItem)
    {
        for(int i = 0; i < deck.Count; i++)
        {
            if(deck[i].ToString().Equals(deckItem.ToString()))
            {

                if (deckItem.getE1() != null && !deckItem.getE1().Equals(""))
                    creditsLeft += map[deckItem.getE1()];

                if (deckItem.getE2() != null && !deckItem.getE2().Equals(""))
                    creditsLeft += map[deckItem.getE2()];

                deck.RemoveAt(i);

                string b1 = deckItem.getB1();
                string b2 = deckItem.getB2();

                colors[b1]--;
                colors[b2]--;

                return;
            }
        }
    }

    public void removeAt(int index)
    {
        deck.RemoveAt(index);
    }

    public DeckItem get(int index)
    {
        return deck[index];
    }


    public List<DeckItem> getDeckList()
    {
        return deck;
    }

    public int getCreditsLeft()
    {
        return creditsLeft;
    }

    public DeckItem draw()
    {
        if(currentItem >= deck.Count)
        {
            currentItem = 0;
            shuffle();
        }

        DeckItem item = deck[currentItem];
        currentItem++;

        return item;
    }

    public void shuffle()
    {
        for (int i = (deck.Count - 1); i > 0; i--)
        {
            int r = Random.Range(0, i);
            DeckItem tmp = deck[i];
            deck[i] = deck[r];
            deck[r] = tmp;
        }
    }

    public bool isLegal()
    {
        legal = true;

        if(deck.Count < minSize || deck.Count > maxSize)
        {
            legalText = "Bad Deck Size";
            legal = false;
        }

        if(creditsLeft < 0)
        {
            legalText = "Not enough credits";
            legal = false;
        }

        foreach(string s in colors.Keys)
        {
            if (colors[s] <= 0)
            {
                legalText = "Color " + s + " has no representation";
                legal = false;
            }              
        }


        if(legal == true)
        {
            legalText = "legal";
        }

        return legal;
    }

    public string getLegalText()
    {
        return legalText;
    }

    override
    public string ToString()
    {
        return deck.ToString();
    }
}
