﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;

public class DeckManager : MonoBehaviour {

    private string player;

    private Deck deck;
    private DeckItem outside;
    private GameObject sample1;
    private GameObject sample2;
    private GameObject sampleEffect1;
    private GameObject sampleEffect2;

    void Start()
    {
        player = "P1";
        deck = new Deck();
        outside = new DeckItem();
    }

    void Update()
    {
             
    }


    public void setPlayer(string player)
    {
        this.player = player;
    }

    public void loadDeckItem()
    {
        setOutsideB1AndConstruct(outside.getB1());
        setOutsideB2AndConstruct(outside.getB2());
        setOutsideE1AndConstruct(outside.getE1());
        setOutsideE2AndConstruct(outside.getE2());
    }

    public void setOutsideB1(string color1)
    {
        outside.setB1(color1);
       
    }

    public void setOutsideB2(string color2)
    {
        outside.setB2(color2);

    }

    public void setOutsideE1(string effect1)
    {
        outside.setE1(effect1);

    }

    public void setOutsideE2(string effect2)
    {
        outside.setE2(effect2);

    }

    public void setOutsideB1AndConstruct(string color1)
    {
        setOutsideB1(color1);

        if (color1.Equals("red"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(4, -2), Quaternion.identity);
        else if (color1.Equals("blue"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(4, -2), Quaternion.identity);
        else if (color1.Equals("green"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(4, -2), Quaternion.identity);
        else if (color1.Equals("yellow"))
            sample1 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(4, -2), Quaternion.identity);
    }

    public void setOutsideB2AndConstruct(string color2)
    {
        setOutsideB2(color2);

        if (color2.Equals("red"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Red", typeof(GameObject)), new Vector2(5,-2), Quaternion.identity);
        else if (color2.Equals("blue"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Blue", typeof(GameObject)), new Vector2(5, -2), Quaternion.identity);
        else if (color2.Equals("green"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Green", typeof(GameObject)), new Vector2(5, -2), Quaternion.identity);
        else if (color2.Equals("yellow"))
            sample2 = (GameObject)Instantiate(Resources.Load("Prefabs/Yellow", typeof(GameObject)), new Vector2(5, -2), Quaternion.identity);
    }

    public void setOutsideE1AndConstruct(string effect1)
    {
        setOutsideE1(effect1);
        string modName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(effect1);

        if(!effect1.Equals(""))
            sampleEffect1 = (GameObject)Instantiate(Resources.Load("Prefabs/" + modName, typeof(GameObject)), new Vector3(4, -2, -5), Quaternion.identity);     
    }

    public void setOutsideE2AndConstruct(string effect2)
    {
        setOutsideE2(effect2);
        string modName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(effect2);

        if (!effect2.Equals(""))
            sampleEffect2 = (GameObject)Instantiate(Resources.Load("Prefabs/" + modName, typeof(GameObject)), new Vector3(5, -2, -5), Quaternion.identity);
    }

    public void attemptAddColor(string color)
    {
        if (sample1 == null)
            setOutsideB1AndConstruct(color);
        else if (sample2 == null)
            setOutsideB2AndConstruct(color);
    }

    public void attemptAddEffect(string effect)
    {
        if(sampleEffect1 == null)
            setOutsideE1AndConstruct(effect);
        else if (sampleEffect2 == null)
            setOutsideE2AndConstruct(effect);
    }

    public string getOutsideB1()
    {
        return outside.getB1();
    }

    public string getOutsideB2()
    {
        return outside.getB2();
    }

    public string getOutsideE1()
    {
        return outside.getE1();
    }

    public string getOutsideE2()
    {
        return outside.getE2();
    }

    public DeckItem getOutside()
    {
        return outside;
    }

    public Deck getDeck()
    {
        return deck;
    }

    public GameObject getSample1()
    {
        return sample1;
    }

    public GameObject getSample2()
    {
        return sample2;
    }

    public GameObject getSampleEffect1()
    {
        return sampleEffect1;
    }

    public GameObject getSampleEffect2()
    {
        return sampleEffect2;
    }

    public void addDeckElement()
    {

        if (!outside.getB1().Equals("") && !outside.getB2().Equals(""))
        {
            

            DeckItem clone = new DeckItem(outside.getB1(), getOutsideB2(), getOutsideE1(), getOutsideE2());
            deck.add(clone);
            FindObjectOfType<DeckDropDown>().add(clone);
            outside.clear();

            GameObject.Destroy(sample1);
            GameObject.Destroy(sample2);
            GameObject.Destroy(sampleEffect1);
            GameObject.Destroy(sampleEffect2);

            sample1 = null;
            sample2 = null;
            sampleEffect1 = null;
            sampleEffect2 = null;
             
        }
    }

    public void removeDeckElement()
    {
        if (!outside.getB1().Equals("") && !outside.getB2().Equals(""))
        {
            Debug.Log(deck);

            DeckItem actual = outside;
            deck.remove(actual);
            FindObjectOfType<DeckDropDown>().remove(actual);
            outside.clear();

            GameObject.Destroy(sample1);
            GameObject.Destroy(sample2);
            GameObject.Destroy(sampleEffect1);
            GameObject.Destroy(sampleEffect2);
            sample1 = null;
            sample2 = null;
            sampleEffect1 = null;
            sampleEffect2 = null;


        }
    }

    public void clearDeckItem()
    {
        outside.clear();

        GameObject.Destroy(sample1);
        GameObject.Destroy(sample2);
        GameObject.Destroy(sampleEffect1);
        GameObject.Destroy(sampleEffect2);
        sample1 = null;
        sample2 = null;
        sampleEffect1 = null;
        sampleEffect2 = null;
    }

    public void saveDeckPersistent()
    {
        if(deck.isLegal())
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file;

            if (player.Equals("P1"))
                file = File.Open(Application.persistentDataPath + "/DeckSave.dat", FileMode.OpenOrCreate);
            else if (player.Equals("P2"))
                file = File.Open(Application.persistentDataPath + "/DeckSave2.dat", FileMode.OpenOrCreate);
            else
                file = null;

            Debug.Log("" + Application.persistentDataPath);

            DeckData data = new DeckData();

            foreach (Dropdown.OptionData point in FindObjectOfType<DeckDropDown>().GetComponent<Dropdown>().options)
            {
                data.getDeck().Add(point.text);
            }


            bf.Serialize(file, data);
            file.Close();
        }
        else
        {
            GameObject comboObject = (GameObject)Instantiate(Resources.Load("Prefabs/ErrorMessage", typeof(GameObject)), new Vector3(0,0,0), Quaternion.identity);
            Debug.Log(comboObject == null);
            Debug.Log(comboObject.transform.Find("errorDescriptionText") == null);
            comboObject.transform.Find("errorDescriptionText").GetComponent<Text>().text = deck.getLegalText();
        }



    }

    public void loadDeckPersistent()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file;

        if (player.Equals("P1"))
            file = File.Open(Application.persistentDataPath + "/DeckSave.dat", FileMode.Open);
        else if (player.Equals("P2"))
            file = File.Open(Application.persistentDataPath + "/DeckSave2.dat", FileMode.Open);
        else
            file = null;

        DeckData data = (DeckData)bf.Deserialize(file);
        file.Close();

        FindObjectOfType<DeckDropDown>().GetComponent<Dropdown>().options.Clear();
        deck = new Deck();

        foreach (string s in data.getDeck())
        {
            FindObjectOfType<DeckDropDown>().GetComponent<Dropdown>().options.Add(new Dropdown.OptionData(s));

            string[] split = s.Split(',');
            DeckItem splitItem = new DeckItem(split[0], split[2], split[1], split[3]);
            deck.add(splitItem);
        }

    }

    public Deck loadDeckForGame(int player)
    {
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file;

        if (player == 1)
            file = File.Open(Application.persistentDataPath + "/DeckSave.dat", FileMode.Open);
        else if (player == 2)
            file = File.Open(Application.persistentDataPath + "/DeckSave2.dat", FileMode.Open);
        else
            file = null;

        DeckData data = (DeckData)bf.Deserialize(file);
        file.Close();

        deck = new Deck();

        foreach (string s in data.getDeck())
        {
            string[] split = s.Split(',');
            DeckItem splitItem = new DeckItem(split[0], split[2], split[1], split[3]);
            deck.add(splitItem);
        }

        return deck;
    }

    public Deck loadDeckForGame2()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/DeckSave2.dat", FileMode.Open);

        DeckData data = (DeckData)bf.Deserialize(file);
        file.Close();

        deck = new Deck();

        foreach (string s in data.getDeck())
        {
            string[] split = s.Split(',');
            DeckItem splitItem = new DeckItem(split[0], split[2], split[1], split[3]);
            deck.add(splitItem);
        }

        return deck;
    }

    public void newDeck()
    {
        deck = new Deck();
    }

    [System.Serializable]
	class DeckData
    {
        private List<string> deck;

        public DeckData()
        {
            deck = new List<string>();
        }

        public void setDeck(List<string> deck)
        {
            this.deck = deck;
        }

        public List<string> getDeck()
        {
            return deck;
        }

    }
    
	
}
